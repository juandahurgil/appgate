-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.5.12-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para appgate
CREATE DATABASE IF NOT EXISTS `appgate` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `appgate`;

-- Volcando estructura para tabla appgate.hibernate_sequence
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla appgate.operandos_usuario
CREATE TABLE IF NOT EXISTS `operandos_usuario` (
  `id` int(11) NOT NULL,
  `operando` int(11) DEFAULT NULL,
  `procesado_operando` varchar(255) DEFAULT NULL,
  `tipo_operando` varchar(255) DEFAULT NULL,
  `sesion_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfrtxhv6dp9p36jt6iskcoimqp` (`sesion_usuario`),
  CONSTRAINT `FKfrtxhv6dp9p36jt6iskcoimqp` FOREIGN KEY (`sesion_usuario`) REFERENCES `sesiones_usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla appgate.sesiones_usuario
CREATE TABLE IF NOT EXISTS `sesiones_usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
