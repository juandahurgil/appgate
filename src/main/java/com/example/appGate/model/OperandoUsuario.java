
package com.example.appGate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "operandos_usuario")
public class OperandoUsuario {
	
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "OPERANDO")
	private Integer operando;
	
	@Column(name = "TIPO_OPERANDO")
	private String tipoOperando;
	
	@Column(name = "PROCESADO_OPERANDO")
	private String procesadoOperando;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SESION_USUARIO")
	private SessionUsuario sessionUsuario;
	
	public OperandoUsuario() {
	}
	
	public OperandoUsuario(Integer operando, String tipoOperando, String procesadoOperando,
	        SessionUsuario sessionUsuario) {
		this.operando = operando;
		this.tipoOperando = tipoOperando;
		this.procesadoOperando = procesadoOperando;
		this.sessionUsuario = sessionUsuario;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getOperando() {
		return operando;
	}
	
	public void setOperando(Integer operando) {
		this.operando = operando;
	}
	
	public String getTipoOperando() {
		return tipoOperando;
	}
	
	public void setTipoOperando(String tipoOperando) {
		this.tipoOperando = tipoOperando;
	}
	
	public String getProcesadoOperando() {
		return procesadoOperando;
	}
	
	public void setProcesadoOperando(String procesadoOperando) {
		this.procesadoOperando = procesadoOperando;
	}
	
	public SessionUsuario getSessionUsuario() {
		return sessionUsuario;
	}
	
	public void setSessionUsuario(SessionUsuario sessionUsuario) {
		this.sessionUsuario = sessionUsuario;
	}
	
	@Override
	public String toString() {
		return "OperandoUsuario [id=" + id + ", operando=" + operando + ", tipoOperando=" + tipoOperando
		        + ", procesadoOperando=" + procesadoOperando + ", sessionUsuario=" + sessionUsuario + "]";
	}
	
}
