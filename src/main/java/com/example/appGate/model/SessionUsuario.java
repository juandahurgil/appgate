
package com.example.appGate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sesiones_usuario")
public class SessionUsuario {
	
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	public SessionUsuario(String name) {
		this.nombre = name;
	}
	
	public SessionUsuario(Integer id) {
		this.id = id;
	}

	public SessionUsuario() {
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return nombre;
	}
	
	public void setName(String name) {
		this.nombre = name;
	}
	
	@Override
	public String toString() {
		return "SessionUsuario [id=" + id + ", name=" + nombre + "]";
	}
	
}
