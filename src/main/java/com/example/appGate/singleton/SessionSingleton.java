
package com.example.appGate.singleton;

public class SessionSingleton {
	
	private static SessionSingleton instancia;
	private Integer numeroUnicoSession;
	
	private SessionSingleton(Integer numeroUnicoSession) {
		this.numeroUnicoSession = numeroUnicoSession;
	}
	
	public static SessionSingleton getInstancia(Integer numeroUnicoSession) {
		if (instancia == null) {
			instancia = new SessionSingleton(numeroUnicoSession);
		}else {
			instancia.setNumeroUnicoSesion(numeroUnicoSession);
		}
		return instancia;
	}
	
	public Integer getNumeroUnicoSesion() {
		return numeroUnicoSession;
	}
	
	public void setNumeroUnicoSesion(Integer numeroUnicoSession) {
		this.numeroUnicoSession = numeroUnicoSession;
	}
}
