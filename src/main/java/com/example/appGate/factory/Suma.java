
package com.example.appGate.factory;

import java.util.List;

public class Suma extends Calculo {
	
	@Override
	public Double realizarCalculo(List<Integer> operandos) {
		return (double) operandos.stream().reduce(0, (a, b) -> a + b);
		
	}
	
}
