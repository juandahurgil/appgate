package com.example.appGate.factory;

import java.util.List;

public abstract class Calculo {
	
	public abstract Double realizarCalculo(List<Integer> operandos);
	
}
