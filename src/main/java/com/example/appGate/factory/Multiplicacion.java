package com.example.appGate.factory;

import java.util.List;

public class Multiplicacion extends Calculo{

	@Override
	public Double realizarCalculo(List<Integer> operandos) {
		return (double) operandos.stream().reduce(1, (a, b) -> a * b);
	}
	
}
