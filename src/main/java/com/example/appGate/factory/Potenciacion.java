
package com.example.appGate.factory;

import java.util.List;

public class Potenciacion extends Calculo {
	
	@Override
	public Double realizarCalculo(List<Integer> operandos) {
		Double resultado = null;
		for (Integer operando : operandos) {
			if (resultado == null) {
				resultado = operando.doubleValue();
				continue;
			}
			else {
				resultado = Math.pow(resultado, operando.doubleValue());
			}
		}
		return resultado;
	}
}
