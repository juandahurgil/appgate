
package com.example.appGate.factory;

import com.example.appGate.enums.EnumTipoCalculo;

public class FactoriaCalculo {
	
	public static Calculo getCalculo(EnumTipoCalculo tipoCalculo) {
		
		switch (tipoCalculo) {
			case SUMA:
				return new Suma();
			
			case RESTA:
				return new Resta();
			
			case MULTIPLICACION:
				return new Multiplicacion();
			
			case DIVISION:
				return new Division();
			
			case POTENCIACION:
				return new Potenciacion();
			
			default:
				break;
		}
		return null;
	}
	
}
