
package com.example.appGate.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appGate.exception.CalculoFailureException;
import com.example.appGate.exception.SessionFailureException;
import com.example.appGate.model.OperandoUsuario;
import com.example.appGate.model.SessionUsuario;
import com.example.appGate.repository.OperandoUsuarioRepository;

@Service
public class OperandoService {
	
	@Autowired
	private OperandoUsuarioRepository operandoUsuarioRepository;
	
	@Autowired
	private SessionService sessionService;
	
	public void ingresarOperando(Integer operando, String tipoOperando) {
		OperandoUsuario operandoUsuario = new OperandoUsuario(operando, tipoOperando, "N",
		        new SessionUsuario(validarInicioSession(sessionService)));
		operandoUsuarioRepository.save(operandoUsuario);
	}
	
	protected List<Integer> obtenerOperandos() throws SessionFailureException {
		List<Integer> listaOperandos = new ArrayList<>();
		List<OperandoUsuario> operandos = operandoUsuarioRepository
		        .findBySessionUsuarioIdAndProcesadoOperando(validarInicioSession(sessionService), "N");
		validarListaOperandosVacia(operandos);
		for (OperandoUsuario operando : operandos) {
			listaOperandos.add(operando.getOperando());
			actualizarOperandosProcesados(operando);
		}
		return listaOperandos;
	}

	private void validarListaOperandosVacia(List<OperandoUsuario> operandos) throws CalculoFailureException {
		if (operandos.isEmpty())
			throw new CalculoFailureException("No existen operandos para realizar un calculo");
	}
	
	private Integer validarInicioSession(SessionService sessionService) {
		if (esSessionActiva(sessionService)) {
			return sessionService.getSessionActiva().getNumeroUnicoSesion();
		}
		else {
			throw new SessionFailureException("Se debe iniciar session antes de ingresar un operando");
		}
	}

	private boolean esSessionActiva(SessionService sessionService) {
		return sessionService.getSessionActiva() != null;
	}
	
	protected void actualizarOperandosProcesados(OperandoUsuario operando) {
		operando.setProcesadoOperando("Y");
		operandoUsuarioRepository.save(operando);
	}
}
