
package com.example.appGate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appGate.enums.EnumTipoCalculo;
import com.example.appGate.exception.CalculoFailureException;
import com.example.appGate.factory.Calculo;
import com.example.appGate.factory.FactoriaCalculo;

@Service
public class CalculoService {
	
	@Autowired
	private OperandoService operandoService;
	
	public Double realizarCalculo(String tipoCalculo) {
		Calculo calculo = FactoriaCalculo.getCalculo(validarTipoCalculo(tipoCalculo));
		Double resultado = calculo.realizarCalculo(operandoService.obtenerOperandos());
		operandoService.ingresarOperando(resultado.intValue(), "Calculado");
		return resultado;
	}
	
	private EnumTipoCalculo validarTipoCalculo(String tipoCalculo) {
		if (esValidoTipoCalculo(tipoCalculo)) {
			return EnumTipoCalculo.fromString(tipoCalculo);
		}
		else {
			throw new CalculoFailureException("Se debe enviar un tipo de calculo y debe ser alguno de estas opciones Suma, Resta, Multiplicacion, Division, Potenciacion");
		}
	}
	
	private boolean esValidoTipoCalculo(String tipoCalculo) {
		return tipoCalculo != null && EnumTipoCalculo.fromString(tipoCalculo) != null;
	}
}
