
package com.example.appGate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appGate.model.SessionUsuario;
import com.example.appGate.repository.SessionUsuarioRepository;
import com.example.appGate.singleton.SessionSingleton;

@Service
public class SessionService {
	
	@Autowired
	private SessionUsuarioRepository sessionUsuarioRepository;
	
	private SessionSingleton sessionActiva;
	
	public void crearSession(String nombre) {
		SessionUsuario registroSession = new SessionUsuario(nombre);
		sessionUsuarioRepository.save(registroSession);
		setSessionActiva(SessionSingleton.getInstancia(registroSession.getId()));
	}
	
	public List<SessionUsuario> consultarTodasSessiones() {
		return (List<SessionUsuario>) sessionUsuarioRepository.findAll();
	}
	
	public SessionSingleton getSessionActiva() {
		return sessionActiva;
	}
	
	public void setSessionActiva(SessionSingleton sessionActiva) {
		this.sessionActiva = sessionActiva;
	}
}
