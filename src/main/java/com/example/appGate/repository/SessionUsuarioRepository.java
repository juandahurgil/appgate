
package com.example.appGate.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.appGate.model.SessionUsuario;

public interface SessionUsuarioRepository extends CrudRepository<SessionUsuario, Integer> {
}
