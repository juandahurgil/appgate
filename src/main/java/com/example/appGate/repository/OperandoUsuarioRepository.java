package com.example.appGate.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.appGate.model.OperandoUsuario;

public interface OperandoUsuarioRepository extends CrudRepository<OperandoUsuario, Integer> {
   
	List<OperandoUsuario> findBySessionUsuarioIdAndProcesadoOperando (Integer id, String esProcesado);
   
}
