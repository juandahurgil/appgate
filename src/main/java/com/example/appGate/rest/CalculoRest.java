
package com.example.appGate.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.appGate.exception.CalculoFailureException;
import com.example.appGate.exception.SessionFailureException;
import com.example.appGate.model.SessionUsuario;
import com.example.appGate.pojo.ObjetoRespuesta;
import com.example.appGate.service.CalculoService;
import com.example.appGate.service.OperandoService;
import com.example.appGate.service.SessionService;

@RestController
@RequestMapping("/api")
public class CalculoRest {
	
	@Autowired
	private CalculoService calculoService;
	
	@Autowired
	private SessionService sessionService;
	
	@Autowired
	private OperandoService operandoService;
	
	@GetMapping("/sessiones")
	public List<SessionUsuario> consultarSessiones() {
		return sessionService.consultarTodasSessiones();
	}
	
	@PostMapping("/session")
	@ResponseStatus(HttpStatus.CREATED)
	public ObjetoRespuesta<Void> crearSession(@RequestParam String nombreSession) {
		sessionService.crearSession(nombreSession);
		return new ObjetoRespuesta<>("201", "Se creo la session exitosamente");
	}
	
	@PostMapping("/operando")
	@ResponseStatus(HttpStatus.CREATED)
	public ObjetoRespuesta<Void> crearOperando(@RequestParam Integer operando) {
		try {
			operandoService.ingresarOperando(operando, "Ingreso");
		}
		catch (SessionFailureException s) {
			return new ObjetoRespuesta<>("500", s.getMessage());
		}
		return new ObjetoRespuesta<>("201", "Se agrego exitosamente el operando");
	}
	
	@PostMapping("/calculo")
	@ResponseStatus(HttpStatus.CREATED)
	public ObjetoRespuesta<Void> realizarCalculo(@RequestParam(required = true) String tipoCalculo) {
		Double resultadoCalculo = 0.0;
		try {
			resultadoCalculo = calculoService.realizarCalculo(tipoCalculo);
		}
		catch (SessionFailureException s) {
			return new ObjetoRespuesta<>("500", s.getMessage());
		}
		catch (CalculoFailureException c) {
			return new ObjetoRespuesta<>("500", c.getMessage());
		}
		return new ObjetoRespuesta<>("201", "El resultado del calculo es " + resultadoCalculo.toString());
	}
}
