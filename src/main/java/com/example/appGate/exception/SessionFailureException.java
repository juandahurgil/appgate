
package com.example.appGate.exception;

public class SessionFailureException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public SessionFailureException(String message) {
		super(message);
	}
}
