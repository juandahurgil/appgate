
package com.example.appGate.exception;

public class CalculoFailureException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public CalculoFailureException(String message) {
		super(message);
	}
}
