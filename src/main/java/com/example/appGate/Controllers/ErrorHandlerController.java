
package com.example.appGate.Controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.appGate.pojo.ObjetoRespuesta;

@ControllerAdvice
public class ErrorHandlerController {
	
	@ResponseBody
	@ExceptionHandler(ArithmeticException.class)
	public ObjetoRespuesta<Void> everyException(ArithmeticException a) {
		return new ObjetoRespuesta<>("500", "No se puede dividir por zero");
	}
	
	@ResponseBody
	@ExceptionHandler(NumberFormatException.class)
	public ObjetoRespuesta<Void> everyException(NumberFormatException n) {
		return new ObjetoRespuesta<>("500", "No se puede agregar una letra como operando");
	}
}
