
package com.example.appGate.pojo;

public class ObjetoRespuesta<T> {
	
	private String code;
	private String message;
	private T body;
	
	public ObjetoRespuesta(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public ObjetoRespuesta(T body, String code, String message) {
		this.body = body;
		this.code = code;
		this.message = message;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public T getBody() {
		return body;
	}
	
	public void setBody(T body) {
		this.body = body;
	}
}
