
package com.example.appGate.enums;

public enum EnumTipoCalculo {
	SUMA("Suma"), RESTA("Resta"), MULTIPLICACION("Multiplicacion"), DIVISION("Division"), POTENCIACION("Potenciacion");
	
	private String text;
	
	EnumTipoCalculo(String text) {
		this.text = text;
	}
	
	public String getText() {
		return this.text;
	}
	
	public static EnumTipoCalculo fromString(String text) {
		for (EnumTipoCalculo b : EnumTipoCalculo.values()) {
			if (b.text.equalsIgnoreCase(text)) {
				return b;
			}
		}
		return null;
	}
}
