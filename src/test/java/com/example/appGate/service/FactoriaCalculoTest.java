
package com.example.appGate.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.example.appGate.enums.EnumTipoCalculo;
import com.example.appGate.factory.Calculo;
import com.example.appGate.factory.FactoriaCalculo;

class FactoriaCalculoTest {
	
	@Test
	void debeCalcularOperacionSuma() {
		Calculo calculoSuma = FactoriaCalculo.getCalculo(EnumTipoCalculo.SUMA);
		assertNotNull(calculoSuma.realizarCalculo(getListaOperandos()));
		assertEquals(13, calculoSuma.realizarCalculo(getListaOperandos()));
	}
	
	@Test
	void debeCalcularOperacionResta() {
		Calculo calculoResta = FactoriaCalculo.getCalculo(EnumTipoCalculo.RESTA);
		assertNotNull(calculoResta.realizarCalculo(getListaOperandos()));
		assertEquals(-13, calculoResta.realizarCalculo(getListaOperandos()));
	}
	
	@Test
	void debeCalcularOperacionMultiplicacion() {
		Calculo calculoMultiplicacion = FactoriaCalculo.getCalculo(EnumTipoCalculo.MULTIPLICACION);
		assertNotNull(calculoMultiplicacion.realizarCalculo(getListaOperandos()));
		assertEquals(20, calculoMultiplicacion.realizarCalculo(getListaOperandos()));
	}
	
	@Test
	void debeCalcularOperacionDivision() {
		Calculo calculoDivision = FactoriaCalculo.getCalculo(EnumTipoCalculo.DIVISION);
		assertNotNull(calculoDivision.realizarCalculo(getListaOperandos()));
		assertEquals(5, calculoDivision.realizarCalculo(getListaOperandos()));
	}
	
	@Test
	void debeCalcularOperacionPotenciacion() {
		Calculo calculoPotenciacion = FactoriaCalculo.getCalculo(EnumTipoCalculo.POTENCIACION);
		assertNotNull(calculoPotenciacion.realizarCalculo(getListaOperandos()));
		assertEquals(100, calculoPotenciacion.realizarCalculo(getListaOperandos()));
	}
	
	private List<Integer> getListaOperandos() {
		List<Integer> listaOperandos = new ArrayList<Integer>();
		listaOperandos.add(10);
		listaOperandos.add(2);
		listaOperandos.add(1);
		return listaOperandos;
	}
}
