
package com.example.appGate.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.appGate.model.OperandoUsuario;
import com.example.appGate.model.SessionUsuario;
import com.example.appGate.repository.OperandoUsuarioRepository;
import com.example.appGate.repository.SessionUsuarioRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OperandoUsuarioRepositoryTest {
	
	@Autowired
	private OperandoUsuarioRepository operandoUsuarioRepository;
	
	@Autowired
	private SessionUsuarioRepository sessionUsuarioRepository;
	
	@Before
	public void setUp() throws Exception {
		SessionUsuario sessionUsuario = new SessionUsuario("Juan");
		sessionUsuarioRepository.save(sessionUsuario);
		OperandoUsuario operandoUsuario = new OperandoUsuario(4, "Ingreso", "N", sessionUsuario);
		assertNull(operandoUsuario.getId());
		operandoUsuarioRepository.save(operandoUsuario);
		assertNotNull(operandoUsuario.getId());
	}
	
	@Test
	public void testConsultaOperandos() {
		List<OperandoUsuario> listaOperandos = operandoUsuarioRepository.findBySessionUsuarioIdAndProcesadoOperando(1, "N");
		assertFalse(listaOperandos.isEmpty());
	}
	
}
