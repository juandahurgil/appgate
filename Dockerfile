FROM debian:9-slim

#Configuramos zona horaria
ENV TZ=America/Bogota
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#Copiamos instalador Java
COPY java/jre1.8.0_202 /opt/jre1.8.0_202

#Instalamos Java
RUN chmod 777 -R /opt/jre1.8.0_202/

#Enviamos variable de entorno Java
ENV JAVA_HOME=/opt/jre1.8.0_202
ENV PATH=$JAVA_HOME/bin:$PATH
ENV JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF8

#Ruta del jar
ADD target/AppGate.jar app.jar

#Indicamos los puertos a exponer
EXPOSE 8080 

#Se agrega start.sh
ADD start.sh /opt/start.sh
RUN chmod +x /opt/start.sh
#Indicamos que siempre ejecute el .sh
CMD ["/opt/start.sh"]
