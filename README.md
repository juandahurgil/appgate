**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. El repositorio del codigo se encuentra en bitbucket en una cuenta publica git clone https://juandahurgil@bitbucket.org/juandahurgil/appgate.git
2. El repositorio de la base de datos se encuentra en bitbucket en una cuenta publica git clone https://juandahurgil@bitbucket.org/juandahurgil/appgatemariabd.git

## Funcionamiento general del servicio

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. El proyecto se creo en Java 8 con Sprint boot 2.5.5 utilizando las librerias de sprint boot web, sprint boot test, jpa, mariadb java client, junit, generando siempre cuando se compila un jar con el nombre  AppGate.
2. Se implemento el patron de diseño de creación singleton representado en la clase SessionSingleton.java para tener solo un objeto de instacia de sesión durante el proceso de un usuario de ingresar operandos y calcular resultados.
3. Se implemento el patron de diseño Abstract Factory para producir familias de objetos relacionados sin especificar sis clases concretas, esto se ve representado en la clase calculo que tiene un metodo abstracto "realizarCalculo"
   el cual implementa todas sus clases hijas Suma, Resta, Multiplicacion, Division, Potenciacion con el factory de FatoriaCalculo
4. Para el manejo de Excepciones se quedaron dos propias heredando de RuntimeException para no tenerlas que propagar explicitamente por los metodos
   para hacer el codigo más limpio, tambien se creo un controller central para el manejo de algunas excepciones en la clase ErrorHandlerController.java	
5. Se crearon tres servicios api rest post que se pueden acceder por las url: 
   http://localhost:8080/api/session?nombreSession=juan el servicio que crea las sesiones
   http://localhost:8080/api/operando?operando=5 el servicio que guarda los operandos
   http://localhost:8080/api/calculo?tipoCalculo=Suma el servicio que realiza los calculos
   Los postman se pueden encontrar en la ruta en el repo de appGate con nombre AppGate.postman_collection.json
6. Se instalo una base de datos mariadb 10.5, en el cual se creo una base de datos con el nombre appgate y se crearon dos tablas 
   operandos_usuario, sessiones_usuario, la creacion de la bd se encuentra en el repo de appgate con el nombre de backup.sql
7. Se creo un pipeline en el repo https://juandahurgil@bitbucket.org/juandahurgil/appgate.git con imagen base de maven 3.6.3 con el servicio de docker habilitado con dos step, el primero se encarga de ejecutar
   las pruebas unitarias, compilar y empaquetar en un jar, de la creacion de un docker con un jre1.8.0_202 expuesto por el puerto 8080 y
   con un push hacia un repositorio publico de docker hub 	, por ultimo un after script que ejecuta la herramien check style
   analisis de codigo estatico y codigo smell
   El segundo step tiene una solución de security con la herramienta git secrets que verifica temas sensibles de contraseñas en la aplicación 
8. Se creo un pipeline en el repo https://juandahurgil@bitbucket.org/juandahurgil/appgatemariabd.git con imagen base de maven 3.6.3 con el servicio de docker habilitado con un step, 
   que tiene la base de datos maria db y crea la bd y importa el backup de las tablas creadas.
9. Dentro del repositorio de bitbucket en la seccion de pipelines pueden ver todas las veces que ha corrido los pipelines y todas las funciones
   y steps que ejecuta, este pipeline se corre cada vez que se realiza un cambio en el branch master	
10. Se crearon 5 pruebas unitarias que comprueban el correcto funcionamiento de de los calculos matematicos, que se corren automaticamente con    
   el pipeline
## Instructivo para desplegar local

1. Instalar la base de datos maria db 10.5 e importar el script que se encuentra https://juandahurgil@bitbucket.org/juandahurgil/appgate.git con
   el nombre backup.sql 	
2. Desplegar el  microservicio descargado del repo https://juandahurgil@bitbucket.org/juandahurgil/appgate.git previamente compilandolo con la herramienta
   maven y desplegando el jar generado con el comando -jar AppGate.java y llamar la api rest AppGate.postman_collection.json que se encuentra en la raiz del proyecto

## Instructivo para desplegar docker

1. Primero desplegar el docker donde encuentra una version de la mariadb con el backup que contruye la base de datos
   docker run -d --name appgatemariadb -p 3306:3306 --net=host juandahurgil/appgatemariadb:74bde618b1e7022a90f1a4c511c0498152bd1b3a
2. Segundo desplegar el docker donde encuentra una version del microservicio de appgate con la api rest de los tres servicios solicitados
   docker run -d --name appgate -p 8080:8080 --net=host juandahurgil/appgate:1
   
   NOTA: Los contenedores van a hacer uso de los puertos 8080 y 3306, por lo cual debe asegurarse que estos puertos no se encuentren en uso 
   previamente, de lo contrario el lanzamiento fallara.
   
   NOTA: Se hace uso del argumento --host para que los contenedores utilicen la configuracion de red del equipo host donde se van a lanzar
   
## Preguntas sobre la prueba
   
4. Adicionar un archivo de texto ReadMe que indique la forma en que propone que el
   servicio sea elástico y escalable.

   Bueno partiendo de que el servicio es un microservicio, es una de las principales caracteristicas para pensar en escalbilidad horizontal,
   mi propuesta seria tener un api gateway que reciba las peticiones y un balanceador de carga que apunte al microservicio, de esta manera
   podemos pensar que podemos replicar el microservicio y que el balanceador se encargue de distribuir las cargas, lo segundo el tema de
   descentralizar las bases de datos y que cada microservicio tenga su propia db.

5. En el mismo ReadMe explique qué atributos de calidad serían más relevantes para
   usted y los posibles trade-offs que se puedan presentar. Explique brevemente qué
   estratégia, patrón, arquitectura aplicaría? 

   Rendimiento, es importante crear aplicacion que sean optimas, que desde el principio se tengan en cuenta temas de performance, como procesos más optimos, 
   manejo de memoria y tunning de consultas, para no sufrir en demasiada medida con el crecimiento de la operación del sistema 
   Mantenibilidad es importante crear sistema que persistan en el tiempo, que sea faciles de actualizar y aplicar cambios, para no sufrir del
   el tema de quedarse atras en las versiones de la tecnologia y que sea muy costoso hacer esas migraciones.
   
   Los trade-offs al tener tacticas para estos atributos de calidad, es poder siempre utilizar las tecnologias nuevas y estables que traigan mejores
   beneficios y estar un paso adeltante en temas de permormance de la aplicación que por lo general se comienzan a realizar solo cuando ya el
   sistema presenta complicaciones.
   
   La arquitectura de microservicios es de gran importancia en estos momentos a mi parecer por dos temas, poder individualizar una solucion
   concreta con sus requerimientos especiales de rendimiento y la escalabilidad horizontal que se presenta con cierta facilidad en una arquitectura que esta pensada para este atributo de escabilidad,
   aunque reconozco que este tipo de arquitectura trae tambien sus inconvenientes como temas de mantenibilidad, se puede mitigar de gran manera teniendo
   una filosofia de devops muy madura.
